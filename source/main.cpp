#include "MicroBit.h"
#include "MicroBitUARTService.h"

MicroBit uBit;
MicroBitUARTService *uart;

int connected = 0;

void onConnected(MicroBitEvent e)
{
    uBit.display.scroll("C");
    connected = 1;

    while (connected) {

        int16_t xA = uBit.accelerometer.getX();
        int16_t yA = uBit.accelerometer.getY();
        int16_t zA = uBit.accelerometer.getZ();
        int16_t xC = uBit.compass.getX();
        int16_t yC = uBit.compass.getY();
        int16_t zC = uBit.compass.getZ();

        PacketBuffer buf(12);
        buf[0] = (xA >> (8 * 0)) & 0xff;
        buf[1] = (xA >> (8 * 1)) & 0xff;
        buf[2] = (yA >> (8 * 0)) & 0xff;
        buf[3] = (yA >> (8 * 1)) & 0xff;
        buf[4] = (zA >> (8 * 0)) & 0xff;
        buf[5] = (zA >> (8 * 1)) & 0xff;
        buf[6] = (xC >> (8 * 0)) & 0xff;
        buf[7] = (xC >> (8 * 1)) & 0xff;
        buf[8] = (yC >> (8 * 0)) & 0xff;
        buf[9] = (yC >> (8 * 1)) & 0xff;
        buf[10] = (zC >> (8 * 0)) & 0xff;
        buf[11] = (zC >> (8 * 1)) & 0xff;

        uart->send(buf);

        fiber_sleep(0.00001);

    }
}

void onDisconnected(MicroBitEvent e)
{
    uBit.display.scroll("D");
    uBit.reset();
}

int main()
{
    // Initialise the micro:bit runtime.
    uBit.init();

    // listen for Bluetooth connection state changes
    uBit.messageBus.listen(MICROBIT_ID_BLE, MICROBIT_BLE_EVT_CONNECTED, onConnected);
    uBit.messageBus.listen(MICROBIT_ID_BLE, MICROBIT_BLE_EVT_DISCONNECTED, onDisconnected);
    uart = new MicroBitUARTService(*uBit.ble);

    release_fiber();

}
